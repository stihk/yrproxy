var express =   require('express'),
	request =   require('request'),
    fs      =   require('fs'),
    util    =   require('util'),
    app     =   express();

String.prototype.replaceAll = function (search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};

function toUnixTimestamp(time) {
    if (util.isNumber(time))
        return time;
    if (util.isDate(time))
        return Math.floor(time.getTime() / 1000);
    throw new Error('Cannot parse time: ' + time);
}

app.set('port', process.env.PORT || 80); //Default port is 80
app.set('UpdateTime', process.env.UPDATE_TIME || 1800); //Default update time is 30 minutes

app.get('*', function (req, res) {
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader("Access-Control-Allow-Headers", "X-Requested-With");
    res.setHeader("Data-Origin", "http://www.yr.no/sted/" + req.url + "/varsel.xml");
    res.writeHead(200, { 'Content-Type': 'text/xml' });

    var fileName = (req.url).replaceAll('/', '-').replace('-', '');

    fs.stat('data/' + fileName + '.xml', function (err, stat) {
        if (err == null) {
            var mtime = toUnixTimestamp(stat.mtime);
            var now = Math.floor(new Date().getTime() / 1000);
            var secondsSinceUpdate = now - mtime;
            if (secondsSinceUpdate >= app.get('UpdateTime')) {
                request("http://www.yr.no/sted/" + req.url + "/varsel.xml", function (error, response, body) {
                    if (body.startsWith("<error>")) {
                        res.end(body);
                        return;
                    }
                    fs.writeFile('data/' + fileName + '.xml', body, 'utf8', function(err) {
                        if (err) throw err;
                        console.log(new Date().toLocaleString(), 'Updated', fileName);
                        res.end(body);
                    });
                });
            } else {
                fs.readFile('data/' + fileName + '.xml', 'utf8', function(err, data) {
                    if (err) throw err;
                    console.log(new Date().toLocaleString(), 'Served cached version of', fileName);
                    res.end(data);
                });
            }
        } else if (err.code == 'ENOENT') {
            request("http://www.yr.no/sted/" + req.url + "/varsel.xml", function (error, response, body) {
                if (body.startsWith("<error>")) {
                    res.end(body);
                    return;
                }
                fs.writeFile('data/' + fileName + '.xml', body, 'utf8', function(err) {
                    if (err) throw err;
                    console.log(new Date().toLocaleString(), 'Created', fileName);
                    res.end(body);
                });
            });
        } else {
            console.log('Some other error: ', err.code);
        }
    });
});

app.listen(app.get('port'), function () {
    console.log('YR.no Proxy listening on port ' + app.get('port'));
});